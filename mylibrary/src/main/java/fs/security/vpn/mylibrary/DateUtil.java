package fs.security.vpn.mylibrary;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    private static SimpleDateFormat sInputFormat = new SimpleDateFormat("MMM dd,yyyy", Locale.US);
    private static SimpleDateFormat sOutputFormat = new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.US);
    private static SimpleDateFormat sOutputTimeFormat = new SimpleDateFormat("hh:mm a", Locale.US);
    private static SimpleDateFormat sInputTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    private static SimpleDateFormat sPresentationDateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
    private static SimpleDateFormat sFullDateFormat = new SimpleDateFormat("MMMM dd yyyy", Locale.US);
    private static SimpleDateFormat sParseOutputDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    private static SimpleDateFormat sParseInputDateFormat = new SimpleDateFormat("EEE dd MMM yyyy", Locale.US);


    public static String getFormattedDate(String dateString) {

        if (dateString != null) {
            try {
                Date inputDate = sInputFormat.parse(dateString);
                return sOutputFormat.format(inputDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static String getFormattedParseDate(String dateString) {

        if (dateString != null) {
            try {
                Date inputDate = sFullDateFormat.parse(dateString);
                return sParseOutputDateFormat.format(inputDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static String getFormattedTime(String timeString) {
        if (timeString != null) {
            try {
                Date inputTime = sInputTimeFormat.parse(timeString);
                return sOutputTimeFormat.format(inputTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getFormattedPresentationTime(String dateString) {
        if (dateString != null) {
            try {
                Date inputTime = sInputTimeFormat.parse(dateString);
                return sPresentationDateFormat.format(inputTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getDate(String dateString) {
        if (dateString != null) {
            try {
                Date inputDate = sParseInputDateFormat.parse(dateString);
                Calendar cal = Calendar.getInstance();
                cal.setTime(inputDate);
                DecimalFormat mFormat= new DecimalFormat("00");
                return mFormat.format(cal.get(Calendar.DATE));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static String getDay(String dateString) {
        if (dateString != null) {
            try {
                Date inputDate = sParseInputDateFormat.parse(dateString);
                Calendar cal = Calendar.getInstance();
                cal.setTime(inputDate);
                return cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static String getFullDate(String dateString) {
        if (dateString != null) {
            try {
                Date inputDate = sParseInputDateFormat.parse(dateString);
                return sFullDateFormat.format(inputDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
